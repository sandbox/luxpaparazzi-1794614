<?php
//include 'php/lib.inc.php';

//$path = "NicKoob";
$current_img = $_GET['current_img'];

$images = dirImages ($path);
if ($current_img == "") {
	$current_img = $images[0];
}

/* ------------------------------------------------------------ */
/* ----  Prepare tags                                       --- */
/* ------------------------------------------------------------ */

if (file_exists($pics_xml)) {
	$pics_file_content = getFile ($pics_xml);
	$img_xml_file_info = stat($uri);
} else {
	$pics_file_content = "";
}
$img_a = ""; // previous image
$img_b = ""; // current image
$img_c = ""; // next image to show
foreach ($images as $img) {
	$img_a = $img_b;
	$img_b = $img_c;
	$img_c = $img;
	$navigation .= getImageAtributes($img_a, $img_b, $img_c);
}
$navigation .= getImageAtributes($img_b, $img_c, "");
if (! $img_copyright) { $img_copyright = $img_copyright_default; }


/* ------------------------------------------------------------ */
/* ----  Prepare $img_current_html                          --- */
/* ------------------------------------------------------------ */
$img_current_html = "<tr>";
if ($img_before) {
	$img_current_html .= "<td align=left><h2><a href=\"?uri=$uri&current_img=$img_before\">< - - </a></h2></td>";
} else {
	$img_current_html .= "<td align=left><h2>< - -</h2></td>";
}
	$img_current_html .= "<td align=center> <h4> $img_title </h4> </td>";

if ($img_after) {
	$img_current_html .= "<td align=right><h2><a href=\"?uri=$uri&current_img=$img_after\"> - - ></a></h2></td>";
} else {
	$img_current_html .= "<td align=right><h2>- - ></h2></td>";
}
$img_current_html .= "</tr>
					  <tr>
					 	<td colspan=3  align=center>
					 		<img src=\"".$path."/".$current_img."\" border=3>
					 	</td>
					  </tr>";
$image_table = "
 	<table>
 		$img_current_html
 		<tr>
 			<td align=center colspan=3>
 				<pre><?=$img_description?></pre>
 	
 				<p align=left>&copy;$img_copyright</p>
			</td>
		</tr>
 	</table>
";

// Vars:
// $curr_title $pics_file $img_info $img_title $img_copyright $img_description
// $img_current_html $img_after $img_before $current_img

/* ------------------------------------------------------------ */
/* ----  Function  getImageAtributes                        --- */
/* ------------------------------------------------------------ */
function getImageAtributes ($img_a, $img_b, $img_c) {
	global $pics_file_content;
	global $current_img;
	global $img_title;
	global $img_copyright;
	global $img_description;
	global $img_before;
	global $img_after;
	global $uri;
	global $img_file_info;
	preg_match("@(.*)((\.jpg)|(\.jpeg)|(\.png)|(\.gif))$@i", $img_b, $regs);
	$curr_title = $regs[1];		
	if ($img_b) {
		$img_info = getImageInformation($pics_file_content, $img_b);
		$img_title_temp = getImageDetails($img_info, "TITLE");
		$img_copyright_temp = getImageDetails($img_info, "COPYRIGHT");
		$img_description_temp = getImageDetails($img_info, "DESCRIPTION");
		if (! $img_title_temp) { $img_title_temp = $curr_title; }
		if ($img_b == $current_img) {
			$img_file_info = stat($uri);
			$navigation = "<br>$img_title_temp";
			$img_before = $img_a;
			$img_after = $img_c;
			$img_title = $img_title_temp;
			$img_copyright = $img_copyright_temp;
			$img_description = $img_description_temp;
		} else {
			$navigation = "<br><a href='?uri=$uri&current_img=$img_b'>$img_title_temp</a>";
		}
	}
	return $navigation;
}
?>
