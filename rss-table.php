
<?php
/*
PHP RSS Reader v1.1
By Richard James Kendall
Bugs to richard@richardjameskendall.com
Free to use, please acknowledge me

Original script at http://www.rjk-hosting.co.uk/programs/prog.php?id=7

HTML-output adapted on 23rd Sept. 2004 by Christian Ries for running an example with baseportal.
	To run with baseportal (www.baseportal.de):
	1. upload this script on your server.
	2. Adapt the URL of the wanted RSS feed in the $file variable below
	3. In a baseportal template insert this code at the place where you want the news to appear:
		<perl>out get "http://location_of_php_script/rss_events.php";</perl>

Place the URL of an RSS feed in the $file variable.

The $rss_channel array will be filled with data from the feed,
every RSS feed is different by by and large it should contain:

Array 	{
	[TITLE] = feed title
	[DESCRIPTION] = feed description
	[LINK] = link to their website
	[IMAGE] = Array {
			[URL] = url of image
	   		[DESCRIPTION] = alt text of image
   			}
	[ITEMS] = Array {
   			[0] = Array 	{
					[TITLE] = item title
   					[DESCRIPTION] = item description
	   				[LINK = a link to the story
   					}
			}
	}


By default it retrives the Reuters Oddly Enough RSS feed. The data is put into the array
structure so you can format the information as you see fit.
*/

set_time_limit(0);

$rss_channel = array();
$currently_writing = "";
$main = "";
$item_counter = 0;

function startElement($parser, $name, $attrs) {
   	global $rss_channel, $currently_writing, $main;
   	switch($name) {
   		case "RSS":
   		case "RDF:RDF":
   		case "ITEMS":
   			$currently_writing = "";
   			break;
   		case "CHANNEL":
   			$main = "CHANNEL";
   			break;
   		case "IMAGE":
   			$main = "IMAGE";
   			$rss_channel["IMAGE"] = array();
   			break;
   		case "ITEM":
   			$main = "ITEMS";
   			break;
   		default:
   			$currently_writing = $name;
   			break;
   	}
}

function endElement($parser, $name) {
   	global $rss_channel, $currently_writing, $item_counter;
   	$currently_writing = "";
   	if ($name == "ITEM") {
   		$item_counter++;
   	}
}

function characterData($parser, $data) {
	global $rss_channel, $currently_writing, $main, $item_counter;
	if ($currently_writing != "") {
		switch($main) {
			case "CHANNEL":
				if (isset($rss_channel[$currently_writing])) {
					$rss_channel[$currently_writing] .= $data;
				} else {
					$rss_channel[$currently_writing] = $data;
				}
				break;
			case "IMAGE":
				if (isset($rss_channel[$main][$currently_writing])) {
					$rss_channel[$main][$currently_writing] .= $data;
				} else {
					$rss_channel[$main][$currently_writing] = $data;
				}
				break;
			case "ITEMS":
				if (isset($rss_channel[$main][$item_counter][$currently_writing])) {
					$rss_channel[$main][$item_counter][$currently_writing] .= $data;
				} else {
					//print ("rss_channel[$main][$item_counter][$currently_writing] = $data<br>");
					$rss_channel[$main][$item_counter][$currently_writing] = $data;
				}
				break;
		}
	}
}

$xml_parser = xml_parser_create();
xml_set_element_handler($xml_parser, "startElement", "endElement");
xml_set_character_data_handler($xml_parser, "characterData");

if (! $rss_file_region ) die ('$rss_file_region must be set for rss-table.php line: '.__LINE__. ' initalize it in ../settings.php!' );
if (!($fp = fopen($rss_file_region, "r"))) {

	echo ("<p><font color=red>Le RSS-Feader '$rss_file_region' n'est pas accessible</font></p>");

} else {
	$error = false;
    while ($data = fread($fp, 4096)) {
        if (!xml_parse($xml_parser, $data, feof($fp))) {
           	echo ("<p><font color=red>".sprintf("XML error: %s at line %d",
                        xml_error_string(xml_get_error_code($xml_parser)),
                        xml_get_current_line_number($xml_parser))."</font></p>");
		$error = true;
        }
    }
    if (! $error) {
        xml_parser_free($xml_parser);

        // OUTPUT AS HTML - Just adapt this part for your output to baseportal


        // print ("<i>" . $rss_channel["DESCRIPTION"] . "</i><br><br>");
       // print ("<table width=\"100%\" border=\"0\" cellpadding=\"2\" class=noborder><tr><td class=noborder>");

      //  print ("<div style=\"overflow:auto; width:190px; height:180px;text-align:left;\">");
        if (isset($rss_channel["ITEMS"])) {
            if (count($rss_channel["ITEMS"]) > 0) {
                for($i = 0;$i < count($rss_channel["ITEMS"]);$i++) {

        // <a href="javascript:winOpen('', 400, 500)">
/* 

	<a href="javascript:islekerartDetail('http://www.islekerart.org/cgi-bin/baseportal.pl?htx=/iogneu/details_DE&Id==73', 400, 500 );"  class="small">Kirmesbal zu K�iber</a>	
	
		

*/

                    print ("\n<p style=\"text-align:left;font-size: 8pt\">"
                                . "<b><a style=\"text-align:left; font-size: 8pt\" href=\"javascript:islekerartDetail('" 
                                . $rss_channel["ITEMS"][$i]["LINK"] . "', 400, 500)\">" 
                                . $rss_channel["ITEMS"][$i]["TITLE"] . "</a></b><br>"
                            );
                            
        			
                    print (html_entity_decode($rss_channel["ITEMS"][$i]["DESCRIPTION"]) . "</p>");
                }
            } else {
                print ("<b>Il n'y a pas d'article sur Islekerart.org!</b>");
            }
        }
       // print ("</div></p>");
       // print ("</td></tr></table>");
  	}
}
?>