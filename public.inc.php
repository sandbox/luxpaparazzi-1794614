<?

function image_menu_navi() {
   global $realuri;
   $realuri = ereg_replace("thumbs.htm", "dia.htm", $realuri);
   image_menu();
   navigation();
   image_menu();
}
function image_table() {
	global $image_table;
	echo ($image_table);
}
function navigation() {
	global $navigation;
	echo ($navigation);
}

function showThumbs($maxcols=4, $border=0, $width=100, $height=100) {
	global $images;
	global $path;
	global $base_url;
	global $realuri;

	$col = 0;

	echo "<center><table border='$border' width=$width><tr>";
	foreach ($images as $img) {
		if ($col == $maxcols) {
			echo "</tr><tr>";
			$col = 0;
		}
		$dir_target = "$path/_resampled_".$width."x".$height."/";
		$dir_source = "$path/";
		if (! file_exists ( $dir_target )) {
			mkdir($dir_target);
		}
		if (! file_exists ( $dir_target.$img)) {
			imageResample($dir_source.$img, $dir_target.$img, $width, $height);
		}

		echo "<td><p><a href=\"".$base_url.$realuri."&current_img=$img"."\">
					<img alt=\"".$base_url."\" src=\"".$dir_target.$img."\" width='$width'  border=0><!-- height='$heigt' -->
		     </a></td>";
		$col++;

	}
	echo "</tr></table></center>";
}
function showAgendaIslekerart($fichier) {

    /* folgende 2 Zeilen sind eine Hilfe f�r das Ansprechen der Variablen */
    /*           0  |   1  |  2  |      3     |    4   | 5     */
    /* fields Datum1|Datum2|Titel|Veranstalter|Internet|Id     */

    $filearray = @file($fichier);

    $offset1 = 5;
    $offset2 = 3;
    $line = $offset1;

    /* hier wird die eingelesene Datei Zeile f�r Zeile behandelt */
    while ( $line <= count($filearray)-$offset2 ) {
    	$data = explode("|", trim($filearray[$line])); /* Dies zerlegt eine Zeile (Eintrag) in ihre Bestandteile (Felder) */

        if ( $data[0] == $data[1] ) { $data[1] = ""; }

        /* Ausgabe der Veranstaltungen */

        print( "<tr><td width='90'>" . $data[0] );
        if ( $data[1] != "" ) {
         print( "&nbsp;-<br>" . $data[1] );
        }
        print( "</td>" );

        print( "<td align='center'><b><a href=\"javascript:islekerartDetail('http://www.islekerart.org/cgi-bin/baseportal.pl?htx=/iogneu/details_DE&Id==". $data[5] );
        print( "', 400, 500 );\" class=\"small\">" . $data[2] );
        print( "</a></b>" );
        if ( $data[4] != "" ) { print( "<br><span style='font-size:9pt'>" . $data[4] . "</span>" ); }

        print( "</td><td align='right'>" . $data[3] );
        print( "</td></tr>" );

        $line++;
    }
}
?>