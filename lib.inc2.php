<?
$javascript = '
<script>
function showPicture(id) {
	F1 = window.open("php/fotos_show.php?id="+id,"Picture","width=700,height=700,left=50,top=50,resizable=yes,scrollbars=yes");
	F1.focus();
}

</script>
';
$default_page = 'eac-home.htm';
function imageResample($source, $target, $width, $height) {
    global $images_should_be_resampled;
    if ($images_should_be_resampled) {
	    // Content type
	    //header('Content-type: image/jpeg');

	    // Get new dimensions
	    list($width_orig, $height_orig) = getimagesize($source);

	    if ($width && ($width_orig < $height_orig)) {
	       $width = ($height / $height_orig) * $width_orig;
	    } else {
	       $height = ($width / $width_orig) * $height_orig;
	    }

	    // Resample
	    $image_p = imagecreatetruecolor($width, $height);
	    $image = imagecreatefromjpeg($source);
	    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);


	    imagejpeg($image_p, $target, 100);
	    // Output to screen:
	    // imagejpeg($image_p, null, 100);
    }
}
function dirImages ($path = "./") {
    $files = Array();
    if ($handle = opendir($path)) {
       while (false !== ($file = readdir($handle))) {

           if (preg_match("@((\.jpg)|(\.jpeg)|(\.png)|(\.gif))$@i", $file, $regs))  {
               #echo "<p>$file\n";
               $files[] = "$file";
           }
       }

		// Note that !== did not exist until 4.0.0-RC2
        /* This is the correct way to loop over the directory. */
        /*   while (false !== ($file = readdir($handle))) {
               echo "<p>$file\n";
           }
        */

        /* This is the WRONG way to loop over the directory. */
        /*
           while ($file = readdir($handle)) {
             echo "$file\n";
           }
        */
       closedir($handle);
    }
    sort($files);
    return $files;
}
function getFile ($file) {
        #readfile("main.htm");
        $fp = fopen($file, "r");
        $localText = "";
        while ($line = fgets($fp, 2000)) {
                $localText .= "$line";
        }
        return $localText;
}
function getBody($mtext) {
	    preg_match ("@<body(.*?)>(.*)</body>@si", $mtext, $res);
        return $res[2];
}
function getHead($mtext) {
	    preg_match ("@<head(.*?)>(.*)</head>@si", $mtext, $res);
        return $res[2];
}
function getRefresh($mtext) {
	    preg_match ("@<meta(.*?)refresh(.*?)URL=(.*?)[\">]@si", $mtext, $res);
        return $res[3];
}
function getFrameMain($mtext, $main) {
	    preg_match ("@<frame name=\"$main\"(.*?)src=\"(.*?)\"(.*?)>@si", $mtext, $res);
	    return $res[2];
}
function getTitle($mtext) {
 	    preg_match ("@<title(.*?)>(.*)</title>@si", $mtext, $res);
        return $res[2];
}
function getTopTitle($mtext) {
 	    preg_match ("@<top-title(.*?)>(.*)</top-title>@si", $mtext, $res);
        return $res[2];
}

function getImageInformation($mtext, $filename) {
 	    preg_match ("@<IMG.*?NAME=\"$filename\".*?>(.*?)</IMG>@si", $mtext, $res);
        return $res[1];
}
function getImageDetails($mtext, $tag) {
 	    preg_match ("@<$tag.*?>(.*?)</$tag.*?>@si", $mtext, $res);
        return $res[1];
}
function mkAbsoluteURL($from, $to) {

	#$from = "\/".$from."\/";
	$count = 0;
	$count2 = 0;
	$prepath = "";
	$temp_to = $to;
	$temp_from = $from;
	while (preg_match("@\.\.\/@", $temp_to, $res)) {
	 	$temp_to = preg_replace("@^\.\.\/@", "", $temp_to);
		$count ++;
	}
	while (preg_match("@^(\/.*?)\/@",  $temp_from, $res)) {
        $temp_from = preg_replace("@^(\/.*?)\/@", "/", $temp_from);
		$dirs[$count2] = $res[1];
		$count2 ++;
	}
	for ($i = 0; $i < $count2-$count; $i++) {
		$prepath = $prepath . $dirs[$i];
	}
	$aurl = $prepath."/".$temp_to;
	$aurl = preg_replace("@^\/@", "", $aurl);
	#$aurl =~ s/^\///;

	return $aurl;

}

function getFileData($uri) {
	global $file;
	global $body;
	global $title;
	global $realuri;
	global $toptitle;
	global $dbh;
	global $usedb;
	$file = getFile ($uri);
	$file_info = stat($uri);
	if ($usedb) {
		$row = $dbh->getRow("SELECT * FROM site WHERE uri = '$uri'");
		if ($file_info['ctime'] > $row['ctime']) {
			//      $dbh->query("INSERT");
			die("File $uri not modified: $file_info[ctime] > $row[ctime]");
		}
	}
	$frame_main = getFrameMain($file, "reservoir");
	$redirect = getRefresh($file);
	if ($redirect) {
		$realuri = mkAbsoluteURL("/".$uri, $redirect);
		getFileData($realuri);
	} elseif ($frame_main) {
		$realuri = mkAbsoluteURL("/".$uri, $frame_main);

		getFileData($realuri);
	} else  {
		$realuri = $uri;
		$body = getBody ($file);
		$head = getHead ($file);
		$title = getTitle ($head);
		$toptitle = getTopTitle ($head);
	}
}

function getAllChildIds($id){
	$query = "SELECT id FROM content WHERE parentid = '$id'";
	$childs = array();
	$childs[] = $id;
	$result = mysql_query($query) or die("Query failed : " . mysql_error());
	while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$childs = array_merge($childs, getAllChildIds($line['id']));
		#$childs[] = $line['id'];
		#print "<p>$line[id]";
	}
	return  $childs;
	#return $childs;
}

function getAllParentIds($id){
	$query = "SELECT parentid FROM content WHERE id = '$id'";
	$parents = array();
	$result = mysql_query($query) or die("Query failed : " . mysql_error());

	if ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$parents[] = $line['parentid'];
		$parents = array_merge($parents, getAllParentIds($line['parentid']));
		#$parents[] = $line['id'];
		#print "<p>$line[id]";
	}
	return  $parents;
	#return $parents;
}
function isParent($id){
	global $parents;
	foreach ($parents as $parent) {
		if ($parent == $id) return true;
	}
	return false;
}
function makeMenu($parentid, $styleX = 'left1', $large=false) {
	global $id ;
	global $parents;

	$navigation = "";
	if ($large) {
		$width_navi = "";
		$width_navi_s = "";
		$title_on_link = "title";
		$styleX = "";

	} else {
		$width_navi = "width=\"".$main{'width_navi'}."\"";
		$width_navi_s = "width=\"".($main{'width_navi'}-5)."\"";
		$title_on_link = "small_title";
	}
	$query = "SELECT id, parentid, title, small_title, rel_dir, filename FROM content WHERE parentid = '$parentid' ORDER BY small_title";
	#echo "<p>$query";
	$result = mysql_query($query) or die("Query failed : " . mysql_error());
	while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
		#$navigation .=  "\t$prefix";
		if (!( $id == $line[id]) || ($large)) {
			$navigation .= "<tr><td class=\"$styleX\">�- </td>
						     <td class=\"$styleX\" $width_navi><a href='?id=$line[id]' caption='$line[title]'>$line[$title_on_link]</a></td></tr>\n";
		} else {
			if (! $large) { $styleX = 'left2'; }
			$navigation .= "<tr><td  class=\"$styleX\">�- </td>
						     <td   class=\"$styleX\" $width_navi><b>$line[$title_on_link]</b></td></tr>\n";
		}
		#$navigation .= "<br>\n";
		#$childs = getAllChildIds($line['id']);
		#echo "<p>$line[id] : ";
		$extended = false;
		if ( isParent($line['id']) || $line[parentid] == 0 || $large){
			$navigation2  = "<tr><td class=\"$styleX\">&nbsp;&nbsp;&nbsp;</td><td  class=\"$styleX\"  $width_navi><table  $width_navi_s>";
			#echo "<br>makeMenu($line[id],     $styleX);";
			$navigation3  = makeMenu($line[id],  $styleX, $large);
			#print "<hr>$line[id]<hr>";
			if (! $large) { $styleX = 'left1'; }
			$navigation4  = "</table></td></tr>";
			if ($navigation3 != '') {
				$navigation .= $navigation2.$navigation3.$navigation4;
			}
			$extended = "true";
			#return $navigation;
		}
		#if (($id == $line[id]) || ($line[rel_dir].$line[filename]  == 'index.htm') )
	}

	return $navigation ;
}
/*
$x = $HTTP_GET_VARS['x'];
if ($x) {
        #echo "<p>--> $x <--";
        $file = $x;
} else {
        $file = $default_page;
} */

?>
